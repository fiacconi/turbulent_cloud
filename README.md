*** TURBULENT CLOUD ***

AUTHOR: Davide Fiacconi, Institute of Astronomy, University of Cambridge

EMAIL: fiacconi AT ast.cam.ac.uk

VERSION: 1.3

This code builds the initial conditions (ICs) for a turbulent spherical cloud whose
gas density follows an isothermal profile with a constant density core. The gas is
in near equilibrium through a mixture of thermal pressure and turbulent motions
determined by its Mach number. The turbulent velocity field is calculated from a
power law velocity power spectrum, and it is mixture of both compressive modes
calculated as the gradient of a scalar function, and solenoidal modes, calculated
as the curl of a vector potential. The code is written in C and runs on multiple
threads through OpenMP.

** DEPENDENCIES: **

The code uses several routines from the Gnu Scientific Libraries (GSL). They are
available at the GSL website, http://www.gnu.org/software/gsl/. Before compiling,
GSL must be compiled and installed in the system.

** HOW TO BUILD: **

The src/ directory contains the source code with a Makefile for automatic
compilation. The user only requires a C compiler (e.g. gcc) to build the code, once
the GSL are installed (see ** DEPENDENCES ** above).

The Makefile contains some compiling-time options (also described in the Makefile):

- -DWITHOMP   : this option enables the OpenMP parallelization.
- -DVERBOSE   : this option enables verbose output on stdout.
- -DFIXPOT    : this option allows to add the fixed potential of a customisable SIS.
- -DWITHBH    : this option allows to add a central black hole to the ICs.
- -DASCII     : this option selects the ASCII output format for the ICs.
- -DGADGET2   : this option selects the GADGET2 output format for the ICs.
- -DAREPO     : this option selects the AREPO/GADGET2 output format for the ICs.
- -DTIPSY     : this option selects the standard binary TIPSY/GASOLINE output
                format for the ICs.

Before compiling the code, enable/disable the desired compiling-time options and
set the right paths for the GSL headers and libraries in the variables GSLINC and
GSLLIB, respectively. Then, on UNIX systems, just type

...$ make

and the code should compile. If the compilation is successful, the src/ directory
should contain the "turbulent_cloud" executable file. Typing "make clean" would
erase all the object files, the executable and any output file produced by the code.

** HOW TO RUN: **

To run the code, use the following syntax

...$ ./turbulent_cloud <parameter_file> [OPTIONS]

The code requires a parameter file to run. A template of the parameter file can be
created with the following syntax

...$ ./turbulent_cloud newparam

This creates a new parameter file "template.param" that can be modified to
initialise the desired model. Then, the code can be run to produce the ICs as

...$ ./turbulent_cloud parameter_file_name.param <integer_number_of_OpenMP_threads>

where the last parameter specifies the number of threads used by the OpenMP. This
parameter is required ONLY IF the code was compiled with the -DWITHOMP option,
otherwise is ignored. According to the chosen file format, the code will produce the
file "cloud.ascii" if compiled with -DASCII, the file "cloud.dat" if compiled with
-DGADGET2 or -DAREPO, or the file "cloud.std" if compiled with -DTIPSY. All files 
contains data with the following units:

- [LENGTH] = 1 pc
- [VELOCITY] = 1 km/s
- [MASS] = 2.3262e2 Msol

which implies [TIME] = 0.978 Myr and G = 1 in code units. The .ascii file has the
following structure: a sequence of columns containing the mass, the x, y, and z
position, and the vx, vy, and vz velocity, and the thermal sound speed, where each
row is associated to a particle.

** TO DO **

- Improve parallelisation of the inverse fft loop to use same procs subdivision
  and memory access (if possible)
- Check parallelisation of the velocity field normalisation

** CHANGES: **

V1.0 : initial repository

V1.1 : possibility to add rotation to the cloud

V1.2 : possibility to add a fixed potential SIS, added Arepo compatible output

V1.3 : possibility to add a central black hole
