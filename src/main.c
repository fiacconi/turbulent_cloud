#include "allinc.h"

void banner() {

    fprintf(stdout, "\n");
    fprintf(stdout, "##### #   # ####  ####  #   # #     ##### #   # #####\n");
    fprintf(stdout, "  #   #   # #   # #   # #   # #     #     # # #   #  \n");
    fprintf(stdout, "  #   #   # #  #  #  #  #   # #     ###   #  ##   #  \n");
    fprintf(stdout, "  #    ###  #   # #####  ###  ##### ##### #   #   #  \n");
    fprintf(stdout, "\n");
    fprintf(stdout, " ###  #      ###   #   # ####  \n");
    fprintf(stdout, "#   # #     #   #  #   # #   # \n");
    fprintf(stdout, "#     #     #   #  #   # #   # \n");
    fprintf(stdout, " #### #####  ###    ###  ####  \n");
    fprintf(stdout, "\n");
    fprintf(stdout, "Davide Fiacconi - Institute of Astronomy, Cambridge\n");
    fprintf(stdout, "email: fiacconi AT ast.cam.ac.uk\n");
    fprintf(stdout, "\n");
    return;
}

int main(int argc, char **argv) {

    MODEL MyModel;

    /* CHECK INPUT ARGUMENT */
    if (argc < 2) error_message("ERROR!\nNo parameter file specified!\n");

    if (!strcmp(argv[1], "-h") || !strcmp(argv[1], "--help")) {
        usage();
        exit(EXIT_SUCCESS);
    }
    else if (!strcmp(argv[1], "newparam")) {
        parameter_file_template();
        exit(EXIT_SUCCESS);
    }

#ifdef WITHOMP
    if (argc < 3) error_message("ERROR!\nCode compiled with the OpenMP support, but the number\nof threads has not been specified!");
    MyModel.n_threads = (int)atoi(argv[2]);
    omp_set_num_threads(MyModel.n_threads);
#endif

    banner();

    fprintf(stdout, "\n------------------- MODEL INITIALIZATION ------------------------\n");

    /* READ PARAMETER FILE */
    read_parameter_file(argv[1], &MyModel);

    /* INITILIASE THE MODEL */
    init_model(&MyModel);

    fprintf(stdout, "\n--------------------- PARTICLE POSITIONS ------------------------\n");

    /* INITIALIZE PARTICLES' POSITIONS */
    initialise_particle_positions(&MyModel);

    fprintf(stdout, "\n--------------------- PARTICLE VELOCITIES -----------------------\n");

    /* INITIALIZE PARTICLES' VELOCITIES */
    turbulent_velocity_field(&MyModel);

    if (MyModel.cloud_v_over_sigma > 0.0) rotational_velocity_field(&MyModel);

    fprintf(stdout, "\n----------------------- DUMPING OUTPUTS -------------------------\n");

    /* WRITE THE OUTPUT FILE */
    write_snapshot(&MyModel);

    fprintf(stdout, "\n------------------------ FREEING MEMORY -------------------------\n");

    /* FREE THE ALLOCATED MEMORY */
    free_memory(&MyModel);

    fprintf(stdout, "\n-----------------------------------------------------------------\n\n");

    exit(EXIT_SUCCESS);
}
