#include "allinc.h"

void usage(void) {
    fprintf(stdout, "TO BE WRITTEN!\n");
    return;
}

void parameter_file_template(void) {
    FILE *f;

    if ((f = fopen("template.param", "w")) == NULL) {
        error_message("ERROR!\nCannot open file 'template.param'!");
    }

    fprintf(f, "RandomSeed\t****\t# Seed of the random number generator\n");
    fprintf(f, "NGasPart\t****\t# Number of gas particles in the cloud\n");
    fprintf(f, "CloudMass\t****\t# Mass of the cloud in solar masses\n");
    fprintf(f, "CloudRad\t****\t# Radius of the cloud in parsecs\n");
    fprintf(f, "CloudCore\t****\t# Radius of the cloud core in parsecs\n");
    fprintf(f, "CloudMach\t****\t# Mach number of the cloud turbulence\n");
    fprintf(f, "CloudVOverSigma\t****\t# Circular velocity over velocity dispersion of the cloud\n");
    fprintf(f, "NMesh\t****\t# Linear number of elements (power of 2!) for the mesh used to calculate turbulence\n");
    fprintf(f, "AlphaSpectrum\t****\t# Power spectrum exponent (e.g Kolmogorov=-11/3, Burger=-4)\n");
    fprintf(f, "CompressiveFraction\t****\t# Fraction of compressive mode in velocity magnitude (not power)\n");

#ifdef TIPSY
    fprintf(f, "GravSoftening\t****\t# Gravitational softening for gas particles in parsecs\n");
#endif

#ifdef AREPO
    fprintf(f, "BoxSize\t****\t# Linear box size in parsecs\n");
#endif

#ifdef FIXPOT
    fprintf(f, "FixPotMass\t****\t# Mass of the fixed potential SIS background in solar masses\n");
    fprintf(f, "FixPotRadius\t****\t# Radius of the fixed potential SIS background in parsecs\n");
#endif

#ifdef WITHBH
    fprintf(f, "BlackHoleMass\t****\t# Mass of the central black hole in solar masses\n");
    fprintf(f, "BlackHoleSoft\t****\t# Gravitational softening of the central black hole in parsecs\n");
#endif

    fprintf(f, "\n\n");

    fclose(f);
    fprintf(stdout, "Template parameter file written succesfully!\n");
    return;
}

void read_parameter_file(char *file_param, MODEL *mod) {

    FILE *f;
    char buffer[MAXCHAR], *value, *parameter;

    f = fopen(file_param, "r");
    if (f == NULL) {
        strcpy(buffer, "ERROR!\nCannot open file '");
        strcat(buffer, file_param);
        strcat(buffer, "'");
        error_message(buffer);
    }

    mod->rng = NULL;
    mod->random_seed = mod->n_fft = mod->n_part = mod->memory = 0;
    mod->k_min = mod->k_max = mod->alpha = 0.0;
    mod->f_compressive = -1.0;
    mod->cloud_mass = mod->cloud_radius = mod->cloud_mach =
        mod->cloud_velocity = mod->cloud_sound_speed =
        mod->cloud_dispersion = mod->cloud_core =
        mod->cloud_v_over_sigma = 0.0;
    mod->p = NULL;

#ifdef TIPSY
    mod->softening = 0.0;
#endif

#ifdef AREPO
    mod->box_size = 0.0;
#endif

#ifdef FIXPOT
    mod->fixpot_mass = 0.0;
    mod->fixpot_radius = 0.0;
#endif

#ifdef WITHBH
    mod->bh_mass = 0.0;
    mod->bh_soft = 0.0;
#endif

#ifdef VERBOSE
    fprintf(stdout, "> Reading parameter file '%s'...\n", file_param);
#endif

    /* MAIN LOOP TO READ THE PARAMETER FILE */
    while (!feof(f)) {

        /* READ AN ENTIRE LINE */
        parameter = fgets(buffer, MAXCHAR, f);

		/* SKIP COMMENTS OR EMPTY LINES */
        if (buffer[0] == '#' || buffer[0] == '%' || !strcmp(buffer, "\n")) continue;

		/* READ PARAMETER NAME FIRST AND THEN PARAMETER VALUE */
        parameter = strtok(buffer, " \t");
        value = strtok(NULL, " \t");

        if (!strcmp(parameter, "RandomSeed"))
			mod->random_seed = (unsigned long int)atoi(value);
        else if (!strcmp(parameter, "NGasPart"))
			mod->n_part = (unsigned int)atoi(value);
        else if (!strcmp(parameter, "CloudMass"))
			mod->cloud_mass = (double)atof(value);
        else if (!strcmp(parameter, "CloudRad"))
			mod->cloud_radius = (double)atof(value);
        else if (!strcmp(parameter, "CloudCore"))
    		mod->cloud_core = (double)atof(value);
        else if (!strcmp(parameter, "CloudMach"))
    		mod->cloud_mach = (double)atof(value);
        else if (!strcmp(parameter, "CloudVOverSigma"))
        	mod->cloud_v_over_sigma = (double)atof(value);
        else if (!strcmp(parameter, "NMesh"))
    		mod->n_fft = (int)atoi(value);
        else if (!strcmp(parameter, "AlphaSpectrum"))
        	mod->alpha = (double)atof(value);
        else if (!strcmp(parameter, "CompressiveFraction"))
            mod->f_compressive = (double)atof(value);
#ifdef TIPSY
        else if (!strcmp(parameter, "GravSoftening"))
            mod->softening = (double)atof(value);
#endif
#ifdef AREPO
        else if (!strcmp(parameter, "BoxSize"))
            mod->box_size = (double)atof(value);
#endif
#ifdef FIXPOT
        else if (!strcmp(parameter, "FixPotMass"))
            mod->fixpot_mass = (double)atof(value);
        else if (!strcmp(parameter, "FixPotRadius"))
            mod->fixpot_radius = (double)atof(value);
#endif
#ifdef WITHBH
        else if (!strcmp(parameter, "BlackHoleMass"))
            mod->bh_mass = (double)atof(value);
        else if (!strcmp(parameter, "BlackHoleSoft"))
            mod->bh_soft = (double)atof(value);
#endif
        else fprintf(stderr, "Parameter '%s' not understood, skipping.\n", parameter);
    }

    fclose(f);

#ifdef VERBOSE
    fprintf(stdout, "> Checking input parameters...\n");
#endif

    if (mod->random_seed == 0) error_message("ERROR!\nParameter 'RandomSeed' not specified!");
    if (mod->n_part == 0) error_message("ERROR!\nParameter 'NGasPart' not specified!");
    if (mod->n_fft == 0) error_message("ERROR!\nParameter 'NMesh' not specified!");
    if (mod->cloud_mass == 0.0) error_message("ERROR!\nParameter 'CloudMass' not specified!");
    if (mod->cloud_radius == 0.0) error_message("ERROR!\nParameter 'CloudRadius' not specified!");
    if (mod->cloud_core == 0.0) error_message("ERROR!\nParameter 'CloudCore' not specified!");
    if (mod->cloud_mach == 0.0) error_message("ERROR!\nParameter 'CloudMach' not specified!");
    if (mod->alpha == 0.0) error_message("ERROR!\nParameter 'AlphaSpectrum' not specified!");
    if (mod->f_compressive < 0.0 || mod->f_compressive > 1.0) error_message("ERROR!\nParameter 'CompressiveFraction' must be between 0 and 1!");
#ifdef TIPSY
    if (mod->softening == 0.0) error_message("ERROR!\nParameter 'GravSoftening' not specified!");
#endif
#ifdef AREPO
    if (mod->box_size == 0.0) error_message("ERROR!\nParameter 'BoxSize' not specified!");
#endif
#ifdef FIXPOT
    if (mod->fixpot_mass == 0.0) error_message("ERROR!\nParameter 'FixPotMass' not specified!");
    if (mod->fixpot_radius == 0.0) error_message("ERROR!\nParameter 'FixPotRadius' not specified!");
#endif
#ifdef WITHBH
    if (mod->bh_mass == 0.0) error_message("ERROR!\nParameter 'BlackHoleMass' not specified!");
    if (mod->bh_soft == 0.0) error_message("ERROR!\nParameter 'BlackHoleSoft' not specified!");
#endif

#ifdef VERBOSE
    fprintf(stdout, "> Parameter file '%s' read succesfully!\n", file_param);
#endif

    return;
}

void init_model(MODEL *mod) {

    int n;

    /* INITIALISE THE PARAMETERS */

#ifdef VERBOSE
    fprintf(stdout, "> Initialising the model and the parameters...\n");
#endif

    mod->memory = 0;
    mod->cloud_mass /= UNITS_MSOL;
    mod->cloud_radius /= UNITS_PC;
    mod->cloud_velocity = sqrt(0.5 * mod->cloud_mass / mod->cloud_radius);
#ifdef FIXPOT
    mod->fixpot_mass /= UNITS_MSOL;
    mod->fixpot_radius /= UNITS_PC;
    mod->cloud_velocity = sqrt(pow(mod->cloud_velocity, 2) + 0.5 * mod->fixpot_mass / mod->fixpot_radius);
#endif
    mod->cloud_sound_speed = mod->cloud_velocity / sqrt(1. + (1. + pow(mod->cloud_v_over_sigma, 2)) * pow(mod->cloud_mach, 2));
    mod->cloud_dispersion = mod->cloud_mach * mod->cloud_sound_speed;
    mod->cloud_rotation = mod->cloud_v_over_sigma * mod->cloud_dispersion;
    mod->cloud_temperature = pow(mod->cloud_sound_speed * 1.0e5, 2) * PROTON_MASS * MMW / BOLTZMANN_CONST;
#ifdef TIPSY
    mod->softening /= UNITS_PC;
#endif
#ifdef AREPO
    mod->box_size /= UNITS_PC;
#endif
#ifdef WITHBH
    mod->bh_mass /= UNITS_MSOL;
    mod->bh_soft /= UNITS_PC;
#endif

    n = mod->n_fft;
    while (n%2 == 0 && n>0) n >>= 1;
    if (n != 1) error_message("ERROR!\nParameter 'CloudMesh' has to be a power of two!");

    mod->k_min = PI / mod->cloud_radius;
    mod->k_max = mod->n_fft * 0.5 * mod->k_min;

    /* INITIALISE THE RANDOM GENERATORS */

#ifdef VERBOSE
    fprintf(stdout, "> Initialising the random generator");
#ifdef WITHOMP
    fprintf(stdout, "s");
#endif
    fprintf(stdout, "...\n");
#endif

#ifdef WITHOMP
    mod->rng = (gsl_rng **)malloc(mod->n_threads * sizeof(gsl_rng *));

    #pragma omp parallel
    {
        char buf[240];
        int nid = omp_get_thread_num();
        #pragma omp critical
        {
        	mod->rng[nid] = gsl_rng_alloc(gsl_rng_mt19937);
        }
        if (mod->rng[nid] == NULL) {
            sprintf(buf, "ERROR!\nNot enough memory to create the random number generator on Thread '%d'!", nid);
            error_message(buf);
        }
        gsl_rng_set(mod->rng[nid], mod->random_seed+nid);
    }

    mod->memory += mod->n_threads * sizeof(gsl_rng);
#else
    mod->rng = gsl_rng_alloc(gsl_rng_mt19937);
    if (mod->rng == NULL) error_message("ERROR!\nNot enough memory to create the random number generator!");
    gsl_rng_set(mod->rng, mod->random_seed);
    mod->memory += sizeof(gsl_rng);
#endif

#ifdef VERBOSE
    fprintf(stdout, "> Random generator");
#ifdef WITHOMP
    fprintf(stdout, "s");
#endif
    fprintf(stdout, " allocated succesfully!\n");
    if (mod->memory / 1024. / 1024. > 1.0) {
        fprintf(stdout, "> Total allocated memory: %.2lf MB\n", mod->memory / 1024. / 1024.);
    }
    else {
        fprintf(stdout, "> Total allocated memory: %.2lf KB\n", mod->memory / 1024.);
    }
#endif

    /* INITIALISE PARTICLES' ARRAY */

    mod->p = (PART*)malloc(mod->n_part * sizeof(PART));
#ifdef WITHOMP
    #pragma omp parallel default(shared) private(n)
    {
        int n_part = mod->n_part;
        double cs = mod->cloud_sound_speed;
        #pragma omp for schedule(static)
        for (n=0; n<n_part; ++n) {
            mod->p[n].cs = cs;
        }
    }
#else
    for (n=0; n<mod->n_part; ++n) mod->p[n].cs = mod->cloud_sound_speed;
#endif


    mod->memory += mod->n_part * sizeof(PART);
#ifdef VERBOSE
    fprintf(stdout, "> Particle array allocated succesfully!\n");
    if (mod->memory / 1024. / 1024. > 1.0) {
        fprintf(stdout, "> Total allocated memory: %.2lf MB\n", mod->memory / 1024. / 1024.);
    }
    else {
        fprintf(stdout, "> Total allocated memory: %.2lf KB\n", mod->memory / 1024.);
    }
#endif

    // SUMMARY OF THE MODEL - NO VERBOSITY REQUIRED

    fprintf(stdout, "\n");
    fprintf(stdout, "@> Number of gas particles = %d\n", mod->n_part);
    fprintf(stdout, "@> Cloud mass = %.2e Msol\n", mod->cloud_mass * UNITS_MSOL);
    fprintf(stdout, "@> Cloud radius = %.2f pc\n", mod->cloud_radius * UNITS_PC);
    fprintf(stdout, "@> Cloud core radius = %.2f pc\n", mod->cloud_core * UNITS_PC);
    fprintf(stdout, "@> Cloud temperature = %.2e K\n", mod->cloud_temperature);
    fprintf(stdout, "@> Cloud sound speed = %.2e km/s\n", mod->cloud_sound_speed);
    fprintf(stdout, "@> Core mass = %.2e Msol\n", mod->cloud_mass * UNITS_MSOL / (3. * mod->cloud_radius / mod->cloud_core - 2.));
    fprintf(stdout, "@> Cloud Mach number = %.2f\n", mod->cloud_mach);
    fprintf(stdout, "@> Cloud velocity dispersion = %.2f\n", mod->cloud_dispersion);
    fprintf(stdout, "@> Cloud turbulence dissipation timescale = %.2f Myr\n",
                        mod->cloud_radius / mod->cloud_dispersion / 0.978);
    fprintf(stdout, "@> Cloud Vrot/sigma = %.2f\n", mod->cloud_v_over_sigma);
    fprintf(stdout, "@> Cloud rotational velocity = %.2f\n", mod->cloud_rotation);
#ifdef TIPSY
    fprintf(stdout, "@> Particle gravitational softening = %.2f pc\n", mod->softening);
#endif
#ifdef AREPO
    fprintf(stdout, "@> Simulation box (Arepo) = %.2f pc\n", mod->box_size);
#endif
#ifdef FIXPOT
    fprintf(stdout, "@> Fixed potential SIS mass = %.2e Msol\n", mod->fixpot_mass * UNITS_MSOL);
    fprintf(stdout, "@> Fixed potential SIS radius = %.2f pc\n", mod->fixpot_radius * UNITS_PC);
#endif
#ifdef WITHBH
    fprintf(stdout, "@> Central black hole mass = %.2e Msol\n", mod->bh_mass * UNITS_MSOL);
    fprintf(stdout, "@> Central black hole gravitational softeninig = %.2f pc\n", mod->bh_soft * UNITS_PC);
#endif
    return;
}


void free_memory(MODEL *mod) {

#ifdef VERBOSE
    fprintf(stdout, "> Freeing allocated memory...\n");
#endif

    /* RANDOM GENERATORS */
#ifdef WITHOMP
    #pragma omp parallel
    {
        int nid = omp_get_thread_num();
        #pragma omp critical
        {
            gsl_rng_free(mod->rng[nid]);
        }
    }
    free(mod->rng);
#else
    gsl_rng_free(mod->rng);
#endif

    /* PARTICLES */
    if (mod->n_part > 0 && mod->p != NULL) free(mod->p);

#ifdef VERBOSE
    fprintf(stdout, "> Memory deallocated succesfully!\n");
#endif

    return;
}
