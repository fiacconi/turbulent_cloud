######################################################
#                 turbulent_cloud                    #
# -------------------------------------------------- #
# Davide Fiacconi                                    #
# Institute of Astronomy, University of Cambridge    #
# Madingley Road CB3 0HA                             #
# Cambridge UK                                       #
# email: fiacconi AT ast.cam.ac.uk                   #
# -------------------------------------------------- #
#                 *** Makefile ***                   #
######################################################

######################################################
# -DVERBOSE = TURN ON VERBOSE SCREEN OUTPUT
OPT += -DVERBOSE
######################################################
# -DWITHOMP = ENABLE USAGE OF OpenMP
OPT += -DWITHOMP
######################################################
# -DFIXPOT = ADD FIXED BACKGROUD SIS POTENTIAL
OPT += -DFIXPOT
######################################################
# -DWITHBH = ADD CENTRAL BLACK HOLE
#OPT += -DWITHBH
######################################################
# -DASCII = OUTPUT IN ASCII FORMAT
#OPT += -DASCII
######################################################
# -DGADGET2 = OUTPUT IN GADGET2 FORMAT
#OPT += -DGADGET2
######################################################
# -DTIPSY = OUTPUT IN GASOLINE/TIPSY FORMAT
#OPT += -DTIPSY
######################################################
# -DAREPO = OUTPUT IN AREPO FORMAT (MINOR CHANGE OF G2)
OPT += -DAREPO
######################################################

######################################################
# GSL SPECIFIC VARIABLES
GSLINC=-I/usr/include
GSLLIB=-L/usr/lib64 -lgsl -lgslcblas

######################################################
# COMPILER VARIABLES AND EXECUTABLE NAME
CC=gcc
OPTIONS=-O3
EXEC=turbulent_cloud

######################################################
# !!!!!!!! DO NOT NEED TO CHANGE BELOW HERE !!!!!!!! #
######################################################

ifeq (WITHOMP,$(findstring WITHOMP,$(OPT)))
OPTIONS += -fopenmp
endif

ALLINC=-I. $(GSLINC)
CFLAGS=-Wall $(ALLINC) $(OPTIONS) $(OPT) -c
ALLLIBS=$(GSLLIB) -lm

SOURCES = main.c init.c io.c turbulence.c allocate.c positions.c

ifeq (ASCII,$(findstring ASCII,$(OPT)))
SOURCES += io_routines/io_ascii.c
else ifeq (GADGET2,$(findstring GADGET2,$(OPT)))
SOURCES += io_routines/io_gadget.c
else ifeq (AREPO,$(findstring AREPO,$(OPT)))
SOURCES += io_routines/io_gadget.c
else ifeq (TIPSY,$(findstring TIPSY,$(OPT)))
SOURCES += io_routines/io_tipsy.c
endif

OBJ = $(SOURCES:.c=.o)

all: $(OBJ)
	$(CC) $(OPTIONS) -o $(EXEC) $(OBJ) $(ALLLIBS)

main.o:
	$(CC) $(CFLAGS) main.c

init.o:
	$(CC) $(CFLAGS) init.c

io.o:
	$(CC) $(CFLAGS) io.c

turbulence.o:
	$(CC) $(CFLAGS) turbulence.c

allocate.o:
	$(CC) $(CFLAGS) allocate.c

positions.o:
	$(CC) $(CFLAGS) positions.c

io_routines/io_ascii.o:
	$(CC) $(CFLAGS) -o io_routines/io_ascii.o io_routines/io_ascii.c

io_routines/io_gadget.o:
	$(CC) $(CFLAGS) -o io_routines/io_gadget.o io_routines/io_gadget.c

io_routines/io_tipsy.o:
	$(CC) $(CFLAGS) -o io_routines/io_tipsy.o io_routines/io_tipsy.c

clean:
	rm -f $(EXEC) $(OBJ)
	rm -f cloud.ascii cloud.dat cloud.std
