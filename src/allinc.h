#ifndef __ALLINC_H__
#define __ALLINC_H__

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_fft_complex.h>
#include <gsl/gsl_fft_real.h>

#ifdef WITHOMP
#include <omp.h>
#endif

#define MAXCHAR 1024
#define PI 3.14159265359
#define SQRT2 1.41421356237
#define SQRT3 1.73205080757
#define UNITS_MSOL 2.3262e2
#define UNITS_PC 1.0
#define MSOL_CGS 1.9891e33
#define PC_CGS 3.08567e18
#define PROTON_MASS 1.6726219e-24
#define BOLTZMANN_CONST 1.38064852e-16
#define MMW 0.59

/* STRUCT DEFINITIONS */

typedef struct part {
    double pos[3];
    double vel[3];
    double cs;
} PART;

typedef struct model {

#ifdef WITHOMP
    int n_threads;
    gsl_rng **rng;
#else
    gsl_rng *rng;
#endif

    unsigned long int random_seed;
    int n_fft;
    unsigned int n_part;
    unsigned long int memory;

    double k_min;
    double k_max;
    double alpha;
    double f_compressive;

    double cloud_mass;
    double cloud_radius;
    double cloud_core;
    double cloud_mach;
    double cloud_v_over_sigma;
    double cloud_velocity;
    double cloud_sound_speed;
    double cloud_dispersion;
    double cloud_rotation;
    double cloud_temperature;

#ifdef TIPSY
    double softening;
#endif

#ifdef AREPO
    double box_size;
#endif

#ifdef FIXPOT
    double fixpot_mass;
    double fixpot_radius;
#endif

#ifdef WITHBH
    double bh_mass;
    double bh_soft;
#endif

    PART *p;
} MODEL;

/* FUNCTION PROTOTYPES */

// allocate.c
void error_message(char *s);
void* allocate_1d_array(const int n);
void** allocate_2d_array(const int n);
void*** allocate_3d_array(const int n);
void free_1d_array(void *array);
void free_2d_array(void *array);
void free_3d_array(void *array);

// io.c
void write_snapshot(MODEL *mod);

// init.c
void usage(void);
void parameter_file_template(void);
void read_parameter_file(char *file_param, MODEL *mod);
void init_model(MODEL *mod);
void free_memory(MODEL *mod);

// positons.c
void initialise_particle_positions(MODEL *mod);

// turbulence.c
void turbulent_velocity_field(MODEL *mod);
void rotational_velocity_field(MODEL *mod);

// io_routines/io_ascii.c
#ifdef ASCII
void write_ic_ascii(MODEL *mod);
#endif

// io_routines/io_gadget.c
#if defined(GADGET2) || defined(AREPO)
void write_ic_gadget(MODEL *mod);
#endif

// io_routines/io_tipsy.c
#ifdef TIPSY
void write_ic_tipsy(MODEL *mod);
#endif

#endif
