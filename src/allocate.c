#include "allinc.h"

void error_message(char *s) {
    fprintf(stderr, "%s\n", s);
    exit(EXIT_FAILURE);
}

void* allocate_1d_array(const int n) {
    void *array;
    array = (void *)malloc(n * sizeof(double));
    if (array == NULL) error_message("ERROR!\nCannot allocate 1D array");
    return array;
}

void** allocate_2d_array(const int n) {
    void **array;
    size_t i;

    array = (void **)malloc(n * sizeof(void *));
    if (array == NULL) error_message("ERROR!\nCannot allocate 2D array");

    array[0] = (void *)malloc(n * n * sizeof(double));
    if (array[0] == NULL) error_message("ERROR!\nCannot allocate 2D array");

    for (i=1; i<n; ++i) array[i] = (void *)((unsigned char *)array[0] + i * n * sizeof(double));

    return array;
}

void*** allocate_3d_array(const int n) {
    void ***array;
    size_t i;

    array = (void ***)malloc(n * sizeof(void **));
    if (array == NULL) error_message("ERROR!\nCannot allocate 3D array");

    array[0] = (void **)malloc(n * n * sizeof(void *));
    if (array[0] == NULL) error_message("ERROR!\nCannot allocate 3D array");

    for(i=1; i<n; ++i) array[i] = (void **)((unsigned char *)array[0] + i * n * sizeof(void *));

    array[0][0] = (void *)malloc(n * n * n * sizeof(double));
    if (array[0] == NULL) error_message("ERROR!\nCannot allocate 3D array");

    for (i=0; i<n*n; ++i) {
        array[0][i] = (void *)((unsigned char *)array[0][0] + i * n * sizeof(double));
    }

    return array;
}

void free_1d_array(void *array) {
    free(array);
}

void free_2d_array(void *array) {
    void **ta = (void **)array;
    free(ta[0]);
    free(array);
}

void free_3d_array(void *array) {
    void ***ta = (void ***)array;

    free(ta[0][0]);
    free(ta[0]);
    free(array);
}
