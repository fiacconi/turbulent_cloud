#include "../allinc.h"
#include "io_tipsy.h"

void init_header(MODEL *mod, TIPSY_HEADER *h) {
    h->time = 0.0;
    h->nbodies = mod->n_part;
    h->ndim = 3;
    h->nsph = mod->n_part;
    h->ndark = 0;
    h->nstar = 0;
#ifdef WITHBH
    h->nstar = 1;
#endif
    return;
}

int write_header(XDR *xdr, TIPSY_HEADER *h) {
    int pad = 0;

    if(xdr_double(xdr, &(h->time)) != 1) return 0;
    if(xdr_int(xdr, &(h->nbodies)) != 1) return 0;
    if(xdr_int(xdr, &(h->ndim)) != 1) return 0;
    if(xdr_int(xdr, &(h->nsph)) != 1) return 0;
    if(xdr_int(xdr, &(h->ndark)) != 1) return 0;
    if(xdr_int(xdr, &(h->nstar)) != 1) return 0;
    if(xdr_int(xdr, &pad) != 1) return 0;
    return 1;
}

void write_gas(XDR *xdr, TIPSY_GAS *p) {
    xdr_vector(xdr, (char *)p, (sizeof(TIPSY_GAS)/sizeof(float)), sizeof(float), (xdrproc_t)xdr_float);
    return;
}

void write_dark(XDR *xdr, TIPSY_DARK *p) {
    xdr_vector(xdr, (char *)p, (sizeof(TIPSY_DARK)/sizeof(float)), sizeof(float), (xdrproc_t)xdr_float);
    return;
}

void write_star(XDR *xdr, TIPSY_STAR *p) {
    xdr_vector(xdr, (char *)p, (sizeof(TIPSY_STAR)/sizeof(float)), sizeof(float), (xdrproc_t)xdr_float);
    return;
}

void write_ic_tipsy(MODEL *mod) {

    int i, j;
    XDR xdr;
    FILE *f;
    TIPSY_HEADER h;
    TIPSY_GAS gp;
    clock_t t0, t1;
#ifdef WITHBH
    TIPSY_STAR gs;
#endif

    fprintf(stdout, "> Writing TIPSY/GASOLINE initial conditions...\n");
    t0 = clock();

    /* OPEN FILE AND INITIALISE */
    f = fopen("cloud.std", "wb");
    if (f == NULL) error_message("ERROR!\nCannot open file 'could.std' for writing!");
    xdrstdio_create(&xdr, f, XDR_ENCODE);

    /* INITIALISE AND WRITE THE HEADER */
    init_header(mod, &h);
    if (!write_header(&xdr, &h)) error_message("ERROR!\nCannot write the header of the file 'could.std'!");

    /* LOOP ON GAS PARTICLES */
    for (i=0; i<mod->n_part; ++i) {
        gp.mass = mod->cloud_mass / mod->n_part;
        for (j=0; j<3; ++j) {
            gp.pos[j] = mod->p[i].pos[j];
            gp.vel[j] = mod->p[i].vel[j];
        }
        gp.rho = gp.phi = 0.0;
        gp.temp = 1.0e10 * mod->p[i].cs * mod->p[i].cs * 0.59 * PROTON_MASS / BOLTZMANN_CONST;
        gp.hsmooth = mod->softening;
        gp.metals = 0.019;
        write_gas(&xdr, &gp);
    }
    
#ifdef WITHBH
    gs.mass = mod->bh_mass;
    for (j=0; j<3; ++j) {
        gs.pos[j] = 0.0;
        gs.vel[j] = 0.0;
    }
    gs.phi = gs.metals = 0.0;
    gs.eps = mod->bh_soft;
    gs.tform = -1.0;
    write_star(&xdr, &gs);
#endif

#ifdef VERBOSE
    fprintf(stdout, "> Gas particles set to solar metallicity = 0.019\n");
#endif


    t1 = clock();
    fprintf(stdout, "> Writing of the TIPSY/GASOLINE initial conditions: %.6lf s\n", (double)(t1-t0)/CLOCKS_PER_SEC);

    /* CLOSE FILE AND FINALISE */
    xdr_destroy(&xdr);
    fclose(f);

    return;
}
