#include "../allinc.h"
#include "io_gadget.h"

void init_header(MODEL *mod, GADGET_HEADER *h) {
    int j;

    for (j=0; j<6; ++j) {
        h->npart[j] = 0;
        h->npartTotal[j] = 0;
        h->npartTotalHighWord[j] = 0;
        h->mass[j] = 0.0;
    }

    h->npart[0] = mod->n_part;
    h->npartTotal[0] = mod->n_part;
    h->mass[0] = mod->cloud_mass / mod->n_part;

#ifdef WITHBH
    h->npart[5] = 1;
    h->npartTotal[5] = 1;
    h->mass[5] = mod->bh_mass;
#endif

    h->time = 0.0;
    h->redshift = 0.0;
    h->flag_sfr = 0;
    h->flag_feedback = 0;
    h->flag_cooling = 0;
    h->num_files = 1;
    h->BoxSize = 0.0;
#ifdef AREPO
    h->BoxSize = mod->box_size;
#endif
    h->Omega0 = 0.0;
    h->OmegaLambda = 0.0;
    h->HubbleParam = 0.0;
    h->flag_metals = 0;
    h->flag_entropy_instead_u = 0;

    if (mod->n_part <= pow(2., 32.)) {
        for(j=0; j<6; ++j) h->npartTotalHighWord[j] = 0;
    }
    else {
        for(j=0; j<6; ++j) h->npartTotalHighWord[j] = h->npartTotal[j]>>31;
    }

    for (j=0; j<60; ++j) h->fill[j] = 0;

    return;
}

void write_ic_gadget(MODEL *mod) {

    int sizeBox, i;
    FILE *initcond;
    GADGET_HEADER header;
    clock_t t0, t1;
    float u;
    float tmp[3];

    fprintf(stdout, "> Writing GADGET2 initial conditions...\n");
    t0 = clock();

    /* OPEN FILE AND CHECK */
    initcond = fopen("cloud.dat","wb");
    if (initcond == NULL) error_message("ERROR!\nCannot open file 'could.dat' for writing!");

    /* INITIALISE THE HEADER */
    init_header(mod, &header);

    /* WRITE THE HEADER */
    sizeBox = sizeof(GADGET_HEADER);
	fwrite(&sizeBox, sizeof(sizeBox), 1, initcond);
	fwrite(&header, sizeof(GADGET_HEADER), 1, initcond);
	fwrite(&sizeBox, sizeof(sizeBox), 1, initcond);

    /* WRITE POSITIONS */
#ifdef WITHBH
    sizeBox = 3 * (mod->n_part + 1) * sizeof(float);
#else
	sizeBox = 3 * mod->n_part * sizeof(float);
#endif
	fwrite(&sizeBox, sizeof(sizeBox), 1, initcond);
    for (i=0; i<mod->n_part; ++i) {
#ifdef AREPO
        tmp[0] = (float)(mod->p[i].pos[0] + 0.5 * mod->box_size);
        tmp[1] = (float)(mod->p[i].pos[1] + 0.5 * mod->box_size);
        tmp[2] = (float)(mod->p[i].pos[2] + 0.5 * mod->box_size);
#else
        tmp[0] = (float)(mod->p[i].pos[0]);
        tmp[1] = (float)(mod->p[i].pos[1]);
        tmp[2] = (float)(mod->p[i].pos[2]);
#endif
        fwrite(tmp, sizeof(float), 3, initcond);
    }
#ifdef WITHBH
#ifdef AREPO
    tmp[0] = (float)(0.5 * mod->box_size);
    tmp[1] = (float)(0.5 * mod->box_size);
    tmp[2] = (float)(0.5 * mod->box_size);
#else
    tmp[0] = 0.0;
    tmp[1] = 0.0;
    tmp[2] = 0.0;
#endif
    fwrite(tmp, sizeof(float), 3, initcond);
#endif
	fwrite(&sizeBox, sizeof(sizeBox), 1, initcond);

    /* WRITE VELOCITIES */
#ifdef WITHBH
    sizeBox = 3 * (mod->n_part + 1) * sizeof(float);
#else
	sizeBox = 3 * mod->n_part * sizeof(float);
#endif
	fwrite(&sizeBox, sizeof(sizeBox), 1, initcond);
    for (i=0; i<mod->n_part; ++i) {
        tmp[0] = (float)(mod->p[i].vel[0]);
        tmp[1] = (float)(mod->p[i].vel[1]);
        tmp[2] = (float)(mod->p[i].vel[2]);
        fwrite(tmp, sizeof(float), 3, initcond);
    }
#ifdef WITHBH
    tmp[0] = 0.0;
    tmp[1] = 0.0;
    tmp[2] = 0.0;
    fwrite(tmp, sizeof(float), 3, initcond);
#endif
    fwrite(&sizeBox, sizeof(sizeBox), 1, initcond);

    /* WRITE IDs */
#ifdef WITHBH
    sizeBox = (mod->n_part + 1) * sizeof(unsigned int);
#else
    sizeBox = mod->n_part * sizeof(unsigned int);
#endif
    fwrite(&sizeBox, sizeof(sizeBox), 1, initcond);
#ifdef AREPO
    for (i=1; i<=mod->n_part; ++i) fwrite(&(i), sizeof(unsigned int), 1, initcond);
#else
    for (i=0; i<mod->n_part; ++i) fwrite(&(i), sizeof(unsigned int), 1, initcond);
#endif
#ifdef WITHBH
    fwrite(&(i), sizeof(unsigned int), 1, initcond);
#endif
    fwrite(&sizeBox, sizeof(sizeBox), 1, initcond);

    /* WRITE SPH INTERNAL ENERGY */
	sizeBox = mod->n_part * sizeof(float);
    fwrite(&sizeBox, sizeof(sizeBox), 1, initcond);
	for(i=0; i<mod->n_part; ++i) {
        u = (float)(mod->p[i].cs * mod->p[i].cs);
        fwrite(&(u), sizeof(float), 1, initcond);
    }
	fwrite(&sizeBox, sizeof(sizeBox), 1, initcond);

    /* CLOSE FILE */
    fclose(initcond);

    t1 = clock();
    fprintf(stdout, "> Writing of the GADGET2 initial conditions: %.6lf s\n", (double)(t1-t0)/CLOCKS_PER_SEC);

    return;
}
