#include "../allinc.h"

void write_ic_ascii(MODEL *mod) {

    int i;
    FILE *f;
    clock_t t0, t1;

    double m_part = mod->cloud_mass / mod->n_part;

    fprintf(stdout, "> Writing ASCII initial conditions...\n");

    t0 = clock();

    /* WRITE THE GAS */
    f = fopen("cloud.ascii","wb");

    if (f == NULL) error_message("ERROR!\nCannot open file 'could.ascii' for writing!");

    fprintf(f, "#m\tx\ty\tz\tvx\tvy\tvz\tcs\n");
    for(i=0; i<mod->n_part; ++i) fprintf(f, "%e\t%e\t%e\t%e\t%e\t%e\t%e\t%e\n", m_part,
                                        mod->p[i].pos[0], mod->p[i].pos[1], mod->p[i].pos[2],
                                        mod->p[i].vel[0], mod->p[i].vel[1], mod->p[i].vel[2],
                                        mod->p[i].cs);
#ifdef WITHBH
       fprintf(f, "%e\t%e\t%e\t%e\t%e\t%e\t%e\t%e\n", mod->bh_mass, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
#endif
    fclose(f);

    t1 = clock();
    fprintf(stdout, "> Writing of the ASCII initial conditions: %.6lf s\n", (double)(t1-t0)/CLOCKS_PER_SEC);

    return;
}
