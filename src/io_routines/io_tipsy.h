#ifndef __IO_TIPSY_H__
#define __IO_TIPSY_H__

#include <rpc/types.h>
#include <rpc/xdr.h>

typedef struct {
    float mass;
    float pos[3];
    float vel[3];
    float rho;
    float temp;
    float hsmooth;
    float metals;
    float phi;
} TIPSY_GAS;

typedef struct {
    float mass;
    float pos[3];
    float vel[3];
    float eps;
    float phi;
} TIPSY_DARK;

typedef struct {
    float mass;
    float pos[3];
    float vel[3];
    float metals;
    float tform;
    float eps;
    float phi;
} TIPSY_STAR;

typedef struct {
    double time;
    int nbodies;
    int ndim;
    int nsph;
    int ndark;
    int nstar;
} TIPSY_HEADER;

#endif
