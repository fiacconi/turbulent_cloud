#include "allinc.h"

void write_snapshot(MODEL *mod) {

#ifdef ASCII
	write_ic_ascii(mod);
#endif
#if defined(GADGET2) || defined(AREPO)
	write_ic_gadget(mod);
#endif
#ifdef TIPSY
	write_ic_tipsy(mod);
#endif

	return;
}
