#include "allinc.h"

#define REAL(z,i) ((z)[2*(i)])
#define IMAG(z,i) ((z)[2*(i)+1])

/**********************************************************************************/

void free_matrix(double *kx, double *ky, double *kz, MODEL *mod);
void init_turbulent_velocity_field(double **sigma_x, double **sigma_y, double **sigma_z, MODEL *mod);
double velocity_power_spectrum(const double k, const double alpha, const double kmin);
void transform_real_velocity_field(double **sigma_x, double **sigma_y, double **sigma_z, MODEL *mod);
void interpolate_particle_velocities(double *sigma_x, double *sigma_y, double *sigma_z, MODEL *mod);
#ifdef WITHBH
void correct_for_black_hole(MODEL *mod);
#endif
/**********************************************************************************/

void turbulent_velocity_field(MODEL *mod) {

    double *v_field_x, *v_field_y, *v_field_z;

    /* INITIALISE THE VELOCITY FIELD IN FOURIER SPACE */
    init_turbulent_velocity_field(&v_field_x, &v_field_y, &v_field_z, mod);

    /* CREATE AND NORMALISE THE REAL VELOCITY FIELDS IN X, Y, Z */
    transform_real_velocity_field(&v_field_x, &v_field_y, &v_field_z, mod);

    /* INTERPOLARE ORA LE VELOCITA' SULLE POSIZIONI DELLE PARTICELLE !!! */
    interpolate_particle_velocities(v_field_x, v_field_y, v_field_z, mod);

    /* FREE THE MEMORY FOR THE TURBULENT VELOCITY FIELD */
#ifdef VERBOSE
    fprintf(stdout, "> Deallocating memory for velocity field...\n");
#endif
    free_matrix(v_field_x, v_field_y, v_field_z, mod);

    /* MODIFY VELOCITY FIELD TO ACCOUNT FOR CENTRAL BH */
#ifdef WITHBH
    correct_for_black_hole(mod);
#endif

    return;
}

/**********************************************************************************/

#ifdef WITHBH
double bh_softened_force(double r, double bh_mass, double bh_soft) {
        double f, u;

       	u = r / 2.8 / bh_soft;
        f = -32. * bh_mass / pow(r, 2);

       	if (u >= 1) {
                f /= 32.;
        }
	else if (u >= 0.5) {
                f *= (1.2*pow(u, 5) - pow(u, 6)/3. - 1.5*pow(u, 4) + 2*pow(u, 3)/3. - 1./480.);
        }
	else {
              	f *= (pow(u, 6) - 1.2*pow(u, 5) + pow(u, 3)/3.);
        }

        return f;
}

void correct_for_black_hole(MODEL *mod) {
    
    int p;

#ifdef VERBOSE
    fprintf(stdout, "> Augmenting particle velocities owing to central black hole...\n");
#ifdef WITHOMP
    double t0 = omp_get_wtime(), t1;
#else
    clock_t t0 = clock(), t1;
#endif
#endif

#ifdef WITHOMP
    #pragma omp parallel default(shared) private(p)
    {
#endif
        PART *pp = mod->p;
        int n_part = mod->n_part;
        double r;
        double mbh = mod->bh_mass;
        double eps = mod->bh_soft;
        double sigma2 = pow(mod->cloud_dispersion, 2);
        double f_norm;
        
#ifdef WITHOMP
        #pragma omp for schedule(static)
        for (p=0; p<n_part; ++p) {
#else
        for (p=0; p<n_part; ++p) {
#endif
            r = sqrt(pp[p].pos[0]*pp[p].pos[0] + pp[p].pos[1]*pp[p].pos[1] + pp[p].pos[2]*pp[p].pos[2]);
            
            f_norm = 1. - bh_softened_force(r, mbh, eps) * r * 0.5 / sigma2;
            
            pp[p].vel[0] *= f_norm;
            pp[p].vel[1] *= f_norm;
            pp[p].vel[2] *= f_norm;
        }
#ifdef WITHOMP
    }
#endif

#ifdef VERBOSE
#ifdef WITHOMP
    t1 = omp_get_wtime();
    fprintf(stdout, "> Augmentation of particle velocities owing to central black hole: %.6f s\n", t1 - t0);
#else
    t1 = clock();
    fprintf(stdout, "> Augmentation of particle velocities owing to central black hole: %.6f s\n", (double)(t1 - t0)/CLOCKS_PER_SEC);
#endif
#endif

    return;
}
#endif

void rotational_velocity_field(MODEL *mod) {

    int p;

#ifdef VERBOSE
    fprintf(stdout, "> Augmenting particle velocities by rotational component...\n");
#ifdef WITHOMP
    double t0 = omp_get_wtime(), t1;
#else
    clock_t t0 = clock(), t1;
#endif
#endif

#ifdef WITHOMP
    #pragma omp parallel default(shared) private(p)
    {
#endif
        PART *pp = mod->p;
        int n_part = mod->n_part;
        double ctheta, stheta, R;
        double vrot = mod->cloud_rotation;

#ifdef WITHOMP
        #pragma omp for schedule(static)
        for (p=0; p<n_part; ++p) {
#else
        for (p=0; p<n_part; ++p) {
#endif
            R = sqrt(pp[p].pos[0]*pp[p].pos[0] + pp[p].pos[1]*pp[p].pos[1]);
            ctheta = pp[p].pos[0] / R;
            stheta = pp[p].pos[1] / R;

            pp[p].vel[0] += -1. * vrot * stheta;
            pp[p].vel[1] += vrot * ctheta;
        }
#ifdef WITHOMP
    }
#endif

#ifdef VERBOSE
#ifdef WITHOMP
    t1 = omp_get_wtime();
    fprintf(stdout, "> Augmentation of particle velocities by rotational component: %.6f s\n", t1 - t0);
#else
    t1 = clock();
    fprintf(stdout, "> Augmentation of particle velocities by rotational component: %.6f s\n", (double)(t1 - t0)/CLOCKS_PER_SEC);
#endif
#endif

    return;
}

/**********************************************************************************/

void free_matrix(double *x, double *y, double *z, MODEL *mod) {

    free(x);
    free(y);
    free(z);

    x = y = z = NULL;

    mod->memory -= 3 * 2 * mod->n_fft * mod->n_fft * mod->n_fft * sizeof(double);
#ifdef VERBOSE
    if (mod->memory / 1024. / 1024. > 1.) {
        fprintf(stdout, "> Total allocated memory: %.2lf MB\n", mod->memory / 1024. / 1024.);
    }
    else {
        fprintf(stdout, "> Total allocated memory: %.2lf KB\n", mod->memory / 1024.);
    }
#endif

    return;
}

/**********************************************************************************/

void init_turbulent_velocity_field(double **sigma_x, double **sigma_y, double **sigma_z, MODEL *mod) {

    int l;
    double *s_x, *s_y, *s_z;

#ifdef VERBOSE
#ifdef WITHOMP
    double t0, t1;
#else
    clock_t t0, t1;
#endif
#endif

#ifdef VERBOSE
    fprintf(stdout, "> Initialiasing 3D arrays for velocity field...\n");
#endif

    s_x = (double *)malloc(mod->n_fft * mod->n_fft * mod->n_fft * 2 * sizeof(double));
    s_y = (double *)malloc(mod->n_fft * mod->n_fft * mod->n_fft * 2 * sizeof(double));
    s_z = (double *)malloc(mod->n_fft * mod->n_fft * mod->n_fft * 2 * sizeof(double));

    mod->memory += 2 * 3 * mod->n_fft * mod->n_fft * mod->n_fft * sizeof(double);

#ifdef VERBOSE
    if (mod->memory / 1024. / 1024. > 1.) {
        fprintf(stdout, "> Total allocated memory: %.2lf MB\n", mod->memory / 1024. / 1024.);
    }
    else {
        fprintf(stdout, "> Total allocated memory: %.2lf KB\n", mod->memory / 1024.);
    }
#endif

#ifdef WITHOMP

#ifdef VERBOSE
    t0 = omp_get_wtime();
#endif

    #pragma omp parallel default(shared) private(l)
    {
        int nid = omp_get_thread_num();
        gsl_rng *local_rng;
        local_rng = mod->rng[nid];
#else

#ifdef VERBOSE
        t0 = clock();
#endif
        gsl_rng *local_rng = mod->rng;
#endif

        int i, j, k, i_neg, j_neg, k_neg, l_neg;
        int n = mod->n_fft;
        double R = mod->cloud_radius;
        double kk = 0.0;
        double PS = 0.0;
        double alpha = mod->alpha;
        double kmin = mod->k_min;
        double ff = mod->f_compressive;
        double A_x, B_x, A_y, B_y, A_z, B_z, phi_re, phi_im;
        double kx, ky, kz;

#ifdef WITHOMP
        #pragma omp for schedule(static)
        for (l=0; l<n*n*(n/2+1); ++l) {
#else
        for (l=0; l<n*n*(n/2+1); ++l) {
#endif
            k = l % n;
            j = (l / n) % n;
            i = (l / n) / n;

            kx = PI / R * ((i<n/2) ? i : i-n);
            ky = PI / R * ((j<n/2) ? j : j-n);
            kz = PI / R * ((k<n/2) ? k : k-n);

            kk = sqrt(pow(kx, 2) + pow(ky, 2) + pow(kz, 2));
            PS = velocity_power_spectrum(kk, alpha, kmin);

            A_x = gsl_ran_gaussian(local_rng, sqrt(PS * 0.5));
            A_y = gsl_ran_gaussian(local_rng, sqrt(PS * 0.5));
            A_z = gsl_ran_gaussian(local_rng, sqrt(PS * 0.5));
            B_x = gsl_ran_gaussian(local_rng, sqrt(PS * 0.5));
            B_y = gsl_ran_gaussian(local_rng, sqrt(PS * 0.5));
            B_z = gsl_ran_gaussian(local_rng, sqrt(PS * 0.5));

            phi_re = gsl_ran_gaussian(local_rng, sqrt(PS * 0.5));
            phi_im = gsl_ran_gaussian(local_rng, sqrt(PS * 0.5));

            if ( (i == 0 || i == n/2) && (j == 0 || j == n/2) && (k == 0 || k == n/2) ) {

                REAL(s_x, l) = ff * (kx * phi_im) + (1. - ff) * (kz * B_y - ky * B_z);
                IMAG(s_x, l) = 0.0;

                REAL(s_y, l) = ff * (ky * phi_im) + (1. - ff) * (kx * B_z - kz * B_x);
                IMAG(s_y, l) = 0.0;

                REAL(s_z, l) = ff * (kz * phi_im) + (1. - ff) * (ky * B_x - kx * B_y);
                IMAG(s_z, l) = 0.0;
            }
            else {

                REAL(s_x, l) = ff * (kx * phi_im) + (1. - ff) * (kz * B_y - ky * B_z);
                IMAG(s_x, l) = ff * (-1. * kx * phi_re) + (1. - ff) * (ky * A_z - kz * A_y);

                REAL(s_y, l) = ff * (ky * phi_im) + (1. - ff) * (kx * B_z - kz * B_x);
                IMAG(s_y, l) = ff * (-1. * ky * phi_re) + (1. - ff) * (kz * A_x - kx * A_z);

                REAL(s_z, l) = ff * (kz * phi_im) + (1. - ff) * (ky * B_x - kx * B_y);
                IMAG(s_z, l) = ff * (-1. * kz * phi_re) + (1. - ff) * (kx * A_y - ky * A_x);

                i_neg = (n-i) % n;
                j_neg = (n-j) % n;
                k_neg = (n-k) % n;
                l_neg = k_neg + n * (j_neg + n * i_neg);

                REAL(s_x, l_neg) = ff * (kx * phi_im) + (1. - ff) * (kz * B_y - ky * B_z);
                IMAG(s_x, l_neg) = -1. * ( ff * (-1. * kx * phi_re) + (1. - ff) * (ky * A_z - kz * A_y) );

                REAL(s_y, l_neg) = ff * (ky * phi_im) + (1. - ff) * (kx * B_z - kz * B_x);
                IMAG(s_y, l_neg) = -1. * ( ff * (-1. * ky * phi_re) + (1. - ff) * (kz * A_x - kx * A_z) );

                REAL(s_z, l_neg) = ff * (kz * phi_im) + (1. - ff) * (ky * B_x - kx * B_y);
                IMAG(s_z, l_neg) = -1. * ( ff * (-1. * kz * phi_re) + (1. - ff) * (kx * A_y - ky * A_x) );
            }
        }
#ifdef WITHOMP
    }
#ifdef VERBOSE
    t1 = omp_get_wtime();
    fprintf(stdout, "> Initialisation of 3D arrays for velocity field: %.6lf s\n", t1-t0);
#endif
#else
#ifdef VERBOSE
    t1 = clock();
    fprintf(stdout, "> Initialisation of 3D arrays for velocity field: %.6lf s\n", (double)(t1-t0)/CLOCKS_PER_SEC);
#endif
#endif

    *sigma_x = s_x;
    *sigma_y = s_y;
    *sigma_z = s_z;
    return;
}

/**********************************************************************************/

double velocity_power_spectrum(const double k, const double alpha, const double kmin) {
    return pow(sqrt(k*k + kmin*kmin), alpha-2.);
}

/**********************************************************************************/

void transform_real_velocity_field(double **sigma_x, double **sigma_y, double **sigma_z, MODEL *mod) {

    int l, n;
    double *s_x, *s_y, *s_z;

#ifdef VERBOSE
#ifdef WITHOMP
    double t0, t1;
#else
    clock_t t0, t1;
#endif
#endif

    n = mod->n_fft;
    double sigma_1D = mod->cloud_dispersion;
    double norm;

    s_x = *sigma_x;
    s_y = *sigma_y;
    s_z = *sigma_z;


#ifdef VERBOSE
    fprintf(stdout, "> Calculating real 3D velocity field...\n");
#ifdef WITHOMP
    t0 = omp_get_wtime();
#else
    t0 = clock();
#endif
#endif

    // INTERNAL DIMENSION
#ifdef WITHOMP
    #pragma omp parallel for default(shared) private(l) schedule(static)
    for (l=0; l<n*n; ++l) {
#else
    for (l=0; l<n*n; ++l) {
#endif
        gsl_fft_complex_radix2_inverse((gsl_complex_packed_array)(&(s_x[l * n * 2])), 1, n);
        gsl_fft_complex_radix2_inverse((gsl_complex_packed_array)(&(s_y[l * n * 2])), 1, n);
        gsl_fft_complex_radix2_inverse((gsl_complex_packed_array)(&(s_z[l * n * 2])), 1, n);
    }

    // INTERMEDIATE DIMENSION
#ifdef WITHOMP
    #pragma omp parallel for default(shared) private(l) schedule(static)
    for (l=0; l<n*n; ++l) {
#else
    for (l=0; l<n*n; ++l) {
#endif
        gsl_fft_complex_radix2_inverse((gsl_complex_packed_array)(&(s_x[(l%n + (l/n) * n * n) * 2])), n, n);
        gsl_fft_complex_radix2_inverse((gsl_complex_packed_array)(&(s_y[(l%n + (l/n) * n * n) * 2])), n, n);
        gsl_fft_complex_radix2_inverse((gsl_complex_packed_array)(&(s_z[(l%n + (l/n) * n * n) * 2])), n, n);
    }

    // EXTERNAL DIMENSION

#ifdef WITHOMP
    #pragma omp parallel for default(shared) private(l) schedule(static)
    for (l=0; l<n*n; ++l) {
#else
    for (l=0; l<n*n; ++l) {
#endif
        gsl_fft_complex_radix2_inverse((gsl_complex_packed_array)(&(s_x[l * 2])), n*n, n);
        gsl_fft_complex_radix2_inverse((gsl_complex_packed_array)(&(s_y[l * 2])), n*n, n);
        gsl_fft_complex_radix2_inverse((gsl_complex_packed_array)(&(s_z[l * 2])), n*n, n);
    }

#ifdef VERBOSE
#ifdef WITHOMP
    t1 = omp_get_wtime();
    fprintf(stdout, "> Calculation of the real 3D velocity field: %.6lf s\n", t1-t0);
#else
    t1 = clock();
    fprintf(stdout, "> Calculation of the real 3D velocity field: %.6lf s\n", (double)(t1-t0)/CLOCKS_PER_SEC);
#endif
#endif

    // NORMALIZATION OF THE VELOCITY FIELDS

    norm = 0.0;
#ifdef WITHOMP
#ifdef VERBOSE
    t0 = omp_get_wtime();
#endif

    #pragma omp parallel for default(shared) private(l) schedule(static) reduction(+:norm)
    for (l=0; l<n*n*n; ++l) {
#else
#ifdef VERBOSE
    t0 = clock();
#endif

    for (l=0; l<n*n*n; ++l) {
#endif
        norm = norm + pow(REAL(s_x, l), 2) + pow(REAL(s_y, l), 2) + pow(REAL(s_z, l), 2);
    }

    norm = sqrt(norm / (n*n*n)) / SQRT3;

#ifdef WITHOMP
    #pragma omp parallel for default(shared) private(l) schedule(static)
    for (l=0; l<n*n*n; ++l) {
#else
    for (l=0; l<n*n*n; ++l) {
#endif
        REAL(s_x, l) = REAL(s_x, l) * sigma_1D / norm;
        REAL(s_y, l) = REAL(s_y, l) * sigma_1D / norm;
        REAL(s_z, l) = REAL(s_z, l) * sigma_1D / norm;
    }

#ifdef VERBOSE
#ifdef WITHOMP
    t1 = omp_get_wtime();
    fprintf(stdout, "> Normalization of the 3D velocity field: %.6lf s\n", t1-t0);
#else
    t1 = clock();
    fprintf(stdout, "> Normalization of the 3D velocity field: %.6lf s\n", (double)(t1-t0)/CLOCKS_PER_SEC);
#endif
#endif

    return;
}

/**********************************************************************************/

void interpolate_particle_velocities(double *sigma_x, double *sigma_y, double *sigma_z, MODEL *mod) {

    int p;

#ifdef VERBOSE
    fprintf(stdout, "> Interpolating the velocity field onto the particle distribution...\n");
#ifdef WITHOMP
    double t0 = omp_get_wtime(), t1;
#else
    clock_t t0 = clock(), t1;
#endif
#endif

#ifdef WITHOMP
    #pragma omp parallel default(shared) private(p)
    {
#endif
        PART *pp = mod->p;
        double dr = 2. * mod->cloud_radius / mod->n_fft;
        double R = mod->cloud_radius;
        int n_part = mod->n_part;
        int n_fft = mod->n_fft;
        double *sx = sigma_x, *sy = sigma_y, *sz = sigma_z;
        int i0, j0, k0, i1, j1, k1;
        double xd, yd, zd;

#ifdef WITHOMP
        #pragma omp for schedule(static)
        for (p=0; p<n_part; ++p) {
#else
        for (p=0; p<n_part; ++p) {
#endif
            i0 = n_fft/2 + (int)(pp[p].pos[0] / dr);
            j0 = n_fft/2 + (int)(pp[p].pos[1] / dr);
            k0 = n_fft/2 + (int)(pp[p].pos[2] / dr);

            if (R + pp[p].pos[0] < (i0 + 0.5)*dr) {
                i1 = i0;
                i0 = i1 - 1;
            }
            else {
                i1 = i0 + 1;
            }

            if (R + pp[p].pos[1] < (j0 + 0.5)*dr) {
                j1 = j0;
                j0 = j1 - 1;
            }
            else {
                j1 = j0 + 1;
            }

            if (R + pp[p].pos[2] < (k0 + 0.5)*dr) {
                k1 = k0;
                k0 = k1 - 1;
            }
            else {
                k1 = k0 + 1;
            }

            xd = ((R + pp[p].pos[0])/dr - i0) / (i1 - i0);
            yd = ((R + pp[p].pos[1])/dr - j0) / (j1 - j0);
            zd = ((R + pp[p].pos[2])/dr - k0) / (k1 - k0);

            pp[p].vel[0] = ((REAL(sx, k0+n_fft*(j0+n_fft*i0))*(1.-xd) +
                                REAL(sx, k0+n_fft*(j0+n_fft*i1))*xd)*(1.-yd) +
                                (REAL(sx, k0+n_fft*(j1+n_fft*i0))*(1.-xd) +
                                REAL(sx, k0+n_fft*(j1+n_fft*i1))*xd)*yd)*(1.-zd) +
                                ((REAL(sx, k1+n_fft*(j0+n_fft*i0))*(1.-xd) +
                                REAL(sx, k1+n_fft*(j0+n_fft*i1))*xd)*(1.-yd) +
                                (REAL(sx, k1+n_fft*(j1+n_fft*i0))*(1.-xd) +
                                REAL(sx, k1+n_fft*(j1+n_fft*i1))*xd)*yd)*zd;
            pp[p].vel[1] = ((REAL(sy, k0+n_fft*(j0+n_fft*i0))*(1.-xd) +
                                REAL(sy, k0+n_fft*(j0+n_fft*i1))*xd)*(1.-yd) +
                                (REAL(sy, k0+n_fft*(j1+n_fft*i0))*(1.-xd) +
                                REAL(sy, k0+n_fft*(j1+n_fft*i1))*xd)*yd)*(1.-zd) +
                                ((REAL(sy, k1+n_fft*(j0+n_fft*i0))*(1.-xd) +
                                REAL(sy, k1+n_fft*(j0+n_fft*i1))*xd)*(1.-yd) +
                                (REAL(sy, k1+n_fft*(j1+n_fft*i0))*(1.-xd) +
                                REAL(sy, k1+n_fft*(j1+n_fft*i1))*xd)*yd)*zd;
            pp[p].vel[2] = ((REAL(sz, k0+n_fft*(j0+n_fft*i0))*(1.-xd) +
                                REAL(sz, k0+n_fft*(j0+n_fft*i1))*xd)*(1.-yd) +
                                (REAL(sz, k0+n_fft*(j1+n_fft*i0))*(1.-xd) +
                                REAL(sz, k0+n_fft*(j1+n_fft*i1))*xd)*yd)*(1.-zd) +
                                ((REAL(sz, k1+n_fft*(j0+n_fft*i0))*(1.-xd) +
                                REAL(sz, k1+n_fft*(j0+n_fft*i1))*xd)*(1.-yd) +
                                (REAL(sz, k1+n_fft*(j1+n_fft*i0))*(1.-xd) +
                                REAL(sz, k1+n_fft*(j1+n_fft*i1))*xd)*yd)*zd;
        }
#ifdef WITHOMP
    }
#endif

#ifdef VERBOSE
#ifdef WITHOMP
    t1 = omp_get_wtime();
    fprintf(stdout, "> Interpolation of the velocity field onto the particle distribution: %.6f s\n", t1 - t0);
#else
    t1 = clock();
    fprintf(stdout, "> Interpolation of the velocity field onto the particle distribution: %.6f s\n", (double)(t1 - t0)/CLOCKS_PER_SEC);
#endif
#endif

    return;
}

/**********************************************************************************/
