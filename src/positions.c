#include <allinc.h>


double gas_radial_position(gsl_rng *rng, const double rcore, const double rtot);
double colatitude_angle(gsl_rng *rng);
double azimuthal_angle(gsl_rng *rng);


void initialise_particle_positions(MODEL *mod) {
    int i;

#ifdef VERBOSE
#ifdef WITHOMP
    double t0, t1;
#else
    clock_t t0, t1;
#endif
#endif

#ifdef VERBOSE
    fprintf(stdout, "> Initialiasing particle positions...\n");
#ifdef WITHOMP
    t0 = omp_get_wtime();
#else
    t0 = clock();
#endif
#endif

#ifdef WITHOMP
    #pragma omp parallel default(shared) private(i)
    {
        int nid = omp_get_thread_num();
        gsl_rng *local_rng;
        local_rng = mod->rng[nid];
#else
        gsl_rng *local_rng = mod->rng;
#endif

        PART *pp = mod->p;
        int n_part = mod->n_part;
        double rc = mod->cloud_core;
        double rt = mod->cloud_radius;
        double r, phi, theta;

#ifdef WITHOMP
        #pragma omp for schedule(static)
        for (i=0; i<n_part; ++i) {
#else
        for (i=0; i<n_part; ++i) {
#endif
            r = gas_radial_position(local_rng, rc, rt);
            theta = colatitude_angle(local_rng);
            phi = azimuthal_angle(local_rng);

            pp[i].pos[0] = r * sin(theta) * cos(phi);
            pp[i].pos[1] = r * sin(theta) * sin(phi);
            pp[i].pos[2] = r * cos(theta);
        }
#ifdef WITHOMP
    }
#endif

#ifdef VERBOSE
#ifdef WITHOMP
    t1 = omp_get_wtime();
    fprintf(stdout, "> Initialisation of particle positions: %.6lf s\n", t1-t0);
#else
    t1 = clock();
    fprintf(stdout, "> Initialisation of particle positions: %.6lf s\n", (double)(t1-t0)/CLOCKS_PER_SEC);
#endif
#endif

    return;
}

double gas_radial_position(gsl_rng *rng, double rcore, double rtot) {
    double y = gsl_rng_uniform(rng);
    if (y * (3. * rtot / rcore - 2.) >= 1.0) {
        return rcore * (2. + y * (3. * rtot / rcore - 2.)) / 3.;
    } else {
        return rcore * pow(y * (3. * rtot / rcore - 2.), 0.333333333);
    }
}

double colatitude_angle(gsl_rng *rng) {
	return acos(1.0 - 2.0 * gsl_rng_uniform(rng));
}

double azimuthal_angle(gsl_rng *rng) {
	return 2.0 * PI * gsl_rng_uniform(rng);
}
